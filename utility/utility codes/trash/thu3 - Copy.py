#https://docs.python.org/3.6/library/tkinter.html
import tkinter as tk
from tkinter import BooleanVar, Checkbutton, Button, Spinbox, Label, IntVar

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()
        
    def returnQuiz(self):
        nQuiz=self.quiz1.get()#this will be a string
        NquesnQuiz=Nques[int(nQuiz)-1]
        print('Quiz=',nQuiz, ' has ',NquesnQuiz, 'questions')
##        self.pick.pack_forget()
##        self.pick1.pack_forget()
        self.ques.pack_forget()
        self.ques1.pack_forget()       

        self.ques=Label(self)
        self.ques["text"]="  Ques"
        self.ques.pack(side="left")
        self.quesState=IntVar()#Used to preset initial ques
        ntemp=Nques[int(nQuiz)-1]
        self.quesState.set(ntemp)
        self.ques1=Spinbox(self, width=0, wrap=1, from_=1,
              to=Nques[int(nQuiz)-1], textvariable=self.quesState)
        self.ques1.pack(side="left")
        self.ques1["command"] = self.returnQues
        
    def returnQues(self):
        print('Ques=',self.ques1.get())
        self.pick=Label(self)
        self.pick["text"]=" Pick"
        self.pick.pack(side='left')
        self.pick1=Spinbox(self,from_=0, to=2, width=0)
        self.pick1.pack(side='left')

        
    def create_widgets(self):
        self.quit = Button(self, text="QUIT", fg="red",
                              command=self.master.destroy)
        self.quit.pack(side="left")
        
        self.click = Button(self)
        self.click["text"] = "click"
        self.click.pack(side="left")
        #quiz is the label and quiz1 is the spinbox
        self.quiz=Label(self)
        self.quiz["text"]="Quiz"
        self.quiz.pack(side="left")
        self.quizState=IntVar()#Used to preset initial quiz
        self.quizState.set(Nquiz)
        self.quiz1=Spinbox(self, from_=1, to=Nquiz,
                    width=0, wrap=1, textvariable=self.quizState)
        self.quiz1.pack(side="left")
        #Make 2 dummy Spinboxes to delete later:
        self.ques=Label(self)
        self.ques["text"]=" Ques"
        self.ques.pack(side='left')
        self.ques1=Spinbox(self,from_=0, to=2, width=0)
        self.ques1.pack(side='left')

        self.quiz1["command"] = self.returnQuiz


               
##        self.chk_state1 = BooleanVar()
##        self.chk_state2 = BooleanVar()
##         
##        self.chk_state1.set(True) #set check state
##        self.chk_state2.set(True) #set check state
##         
##        self.chk1 = Checkbutton(self, text='1', var=self.chk_state1)
##        self.chk2 = Checkbutton(self, text='2', var=self.chk_state2)
##        print(self.chk_state1.get())
##         
##        self.chk1.pack(side="left")
##        self.chk2.pack(side="left")
        
Nques= 3,9,2
Nquiz=len(Nques)
root = tk.Tk()
app = Application(master=root)
app.mainloop()

