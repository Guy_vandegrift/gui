#https://docs.python.org/3.6/library/tkinter.html
import tkinter as tk
from tkinter import (BooleanVar, Checkbutton, Button, Spinbox, Label, IntVar,
Radiobutton)

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()       
    def returnQuiz(self):
        nQuiz=self.quiz1.get()#this will be a string
        NquesnQuiz=Nques[int(nQuiz)-1]
        print('Quiz=',nQuiz, ' has ',NquesnQuiz, 'questions')
        #forget ques and ques1 widgets
        self.ques.pack_forget()
        self.ques1.pack_forget()       
        #create new ques and ques1 widgets
        self.ques=Label(self) #ques for Label
        self.ques["text"]="  Ques"
        self.ques.pack(side="left")
        #set initial state for the quizzes based on Nques
        self.quesState=IntVar()
        ntemp=Nques[int(nQuiz)-1]
        self.quesState.set(ntemp) #ques1 for Spinbox
        self.ques1=Spinbox(self, width=0, wrap=1, from_= 1,
              to=Nques[int(nQuiz)-1], textvariable=self.quesState)
        self.ques1.pack(side="left")
        #Activate the question
        self.ques1["command"] = self.returnQues        
    def returnQues(self):
        print('Ques=',self.ques1.get())
        #print('... Pick ',self.pick.get())
    def LaTeX(self):
        print("Printing exam as a LaTex .tex file")
    def returnPick(self):
        print("We are here in Pick")        
    def create_widgets(self):
        # -- Quit button --
        self.quit = Button(self, text="QUIT", fg="red",
                              command=self.master.destroy)
        self.quit.pack(side="left")        
        # -- LaTeX button --
        self.print = Button(self, text="make\nLaTeX",fg="green",
                            command=self.LaTeX)      
        self.print.pack(side="left")
        # -- ON/OFF button
        self.qPick=IntVar()
        self.qPick.set(False)
        self.pick=Radiobutton(self,text="ON",variable=self.qPick,
              value=True,command=self.returnPick).pack(side="right")
        self.pick=Radiobutton(self,text="OFF",variable=self.qPick,
              value=False,command=self.returnPick).pack(side="right")
##                              
##                              )
##        #self.pick=Checkbutton(self,text="Select\nquestion")
##        self.pick.pack(side='right')



        ##############################################

        #Create quiz Label and quiz1 Spinbox
        self.quiz=Label(self)
        self.quiz["text"]="Quiz"
        self.quiz.pack(side="left")
        self.quizState=IntVar()#Used to preset initial quiz
        self.quizState.set(Nquiz)
        self.quiz1=Spinbox(self, from_=1, to=Nquiz,
                    width=0, wrap=1, textvariable=self.quizState)
        self.quiz1.pack(side="left")

        #Make dummy ques and pick pairs:
        self.ques=Label(self)
        self.ques["text"]=" Ques"
        self.ques.pack(side='left')
        self.ques1=Spinbox(self,from_=0, to=1, width=0)
        self.ques1.pack(side='left')

        self.quiz1["command"] = self.returnQuiz


               
##        self.chk_state1 = BooleanVar()
##        self.chk_state2 = BooleanVar()
##         
##        self.chk_state1.set(True) #set check state
##        self.chk_state2.set(True) #set check state
##         
##        self.chk1 = Checkbutton(self, text='1', var=self.chk_state1)
##        self.chk2 = Checkbutton(self, text='2', var=self.chk_state2)
##        print(self.chk_state1.get())
##         
##        self.chk1.pack(side="left")
##        self.chk2.pack(side="left")
        
Nques= 3,9,2
Nquiz=len(Nques)
root = tk.Tk()
app = Application(master=root)
app.mainloop()


