#https://docs.python.org/3.6/library/tkinter.html
import tkinter as tk
from tkinter import BooleanVar, Checkbutton, Button, Spinbox

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.grid()
        self.create_widgets()

    def returnSpin(self):
        print(self.spin1.get())

    def create_widgets(self):
        
        self.getSpin = tk.Button(self)
        self.getSpin["text"] = "click"
        self.getSpin["command"] = self.returnSpin
        self.getSpin.grid(column=1,row=0)

        self.quit = Button(self, text="QUIT", fg="red",
                              command=self.master.destroy)
        self.quit.grid(column=2,row=0)
        

        self.chk_state1 = BooleanVar()
        self.chk_state2 = BooleanVar()
         
        self.chk_state1.set(True) #set check state
        self.chk_state2.set(True) #set check state
         
        self.chk1 = Checkbutton(self, text='1', var=self.chk_state1)
        self.chk2 = Checkbutton(self, text='2', var=self.chk_state2)
        print(self.chk_state1.get())
         
        self.chk1.grid(column=0, row=1)
        self.chk2.grid(column=1, row=1)
        

        self.spin1=Spinbox(self, from_=0, to=2, width=2)
        self.spin1.grid(column=0, row=2)
        print(self.spin1.get())
        









root = tk.Tk()
app = Application(master=root)
app.mainloop()
