#import tkinter as tk
#from tkinter import (BooleanVar, Checkbutton, Button, Spinbox, Label, IntVar,
#Radiobutton)

import os, sys, csv, re, time, copy, shutil, random#(re not used yet)

PATH2BANK='../bank/'
PATH2COURSES='./courses/'

class Question:
    def __init__(self):
        self.quizName =""
        self.number=0 #
        self.quizType = ""
        self.QA = [] #question and answer
        self.Q=[] #question only
        self.A=[] #answer only
        self.Nrenditions = 1
class Quiz:
    def __init__(self):
        self.quizName =""
        self.quizType = ""
        self.questions = []#Question() clas list
        self.Nquestions=0#number of questions
class UpgradeInfo:  #diluted from CsvInfo in previous code
    def __init__(self):
        self.Nquestions=[]
        self.types=[]
        self.quizNames=[]
        self.Nquizzes=0

#################### -- imported from previous code ########################
def findLatexWhole(pre,string,post):
    #returns list of indices between the pre and post
    #tags.  Whole includes the tags
    mylist=[]
    tag=pre+r'(.*?)'+post
    matches=re.finditer(tag,string,re.S)
    for item in matches:
        mylist.append([item.start(),item.end()])
    #mylist is a lists of two element lists (start/st
    #the two element list is the start/stop point in string
    #mylist is as long as there are instances of the tag
    return mylist  #ends findLatexWhole
def findLatexBetween(pre,strIn,post):
    #returns list of indeces between the pre and post
    #tags.  Between excludes the tags
    mylist=[]
    tag=pre+r'(.*?)'+post
    matches=re.finditer(tag,strIn,re.S)
    for item in matches:
    #To get "between" we subtract out the pre and post
    #part of the tag.  We need to count the backslashes
    #in order to compensate for the extra \ in the \\ escape
        n1=item.start()+len(pre)-pre.count(r'\\')
        n2=item.end()-len(post)+post.count(r'\\')#as before:
    #mylist is a lists of two element lists (start/st
    #the two element list is the start/stop point in string
    #mylist is as long as there are instances of the tag
        mylist.append([n1,n2])
    return mylist                        #ends findLatexBetween
def QA2QandA(string): 
    #Define 4 patterns:
    beginStr=r'\\begin{choices}'
    endStr=r'\\end{choices}'
    CoStr=r'\\CorrectChoice '
    chStr=r'\\choice '
    #get Q=question
    n=string.find('\\begin{choices}')
    Q=string[0:n]
    #get A=string of answers
    #newStr defines the new search parameter as 1st answer
    pat=CoStr+'|'+chStr+'|'+endStr+'(.*?)'#fails-not greedy   
    iterations=re.finditer(pat,string,re.S)
    nList=[0]
    A=[]  
    for thing in iterations:
        nList.append(thing.start())
    #for i in range(len(nList)): #crashes as expected
    for i in range(1,len(nList)-1):
        A.append(string[nList[i]:nList[i+1]])
    return [Q,A]
#def makeQuiz(path,quizName):



        
###################end functions inserted from previus######################



def getWikiquizzes():
    comment='UPGRADE rewritten at each call. Copy and rename before using.'
    UpgradeInfo.quizNames=[] #quizname w/o .tex suffix
    UpgradeInfo.types=[] #'conceptual' or 'numerical'
    UpgradeInfo.Nquestions=[] #Number of questions in quiz
    UpgradeInfo.Nquizzes=0 #Number quizzez in bank
    #get quizNames, types, and Nquestions:
    for quizFile in os.listdir(PATH2BANK): 
        if quizFile.endswith('.tex'):  #quizName
            UpgradeInfo.Nquizzes+=1
            UpgradeInfo.quizNames.append(quizFile[:-4])
            path2quiz=PATH2BANK+quizFile
            quizStr='' #formally called strIn
            with open(path2quiz,'r') as fin:
                for line in fin: 
                    quizStr+=line  #returns entire quiz
                tag=findLatexBetween(r'{\\quiztype}{',quizStr,'}')
                stype=quizStr[tag[0][0]:tag[0][1]]

                UpgradeInfo.types.append(stype)
                tag=findLatexBetween(r'\\begin{questions}',  
                    quizStr, r'\\end{questions}') 
                firstBatch=quizStr[tag[0][0]:tag[0][1]]
                tag=findLatexWhole(r'\\question ',firstBatch, r'\\end{choices}')
                UpgradeInfo.Nquestions.append(len(tag))    
                print(stype, len(tag), quizFile)
##
##
##                if stype=='conceptual':  #stype b/c type is reserved
##                    stype='c'
##                elif stype=='numerical':
##                    stype='n'

            





    print('\n',len(UpgradeInfo.quizNames),'Wikiquizzes found in',PATH2BANK) 
    path=PATH2COURSES+'allBank/UPGRADE.csv'
    with open(path,'w',newline='') as fout:
        wr=csv.writer(fout,delimiter=',')
        
        wr.writerow([comment])
        for row in UpgradeInfo.quizNames:
            wr.writerow([row])
    return UpgradeInfo.quizNames

##def makeQuiz(path,quizName):
##    strIn=""
##    with open(path,'r') as fin:
##      for line in fin:
##        strIn+=line
##        ########### we build Quiz() etc from strIn
##    quiz=Quiz()
##    quiz.quizName=quizName  
##    x=findLatexBetween(r'{\\quiztype}{',strIn,'}')
##    quizType=strIn[x[0][0]:x[0][1]]
##    quiz.quizType=quizType                      
##    x=findLatexBetween(r'\\begin{questions}',  #List of index pairs
##                    strIn,r'\\end{questions}') #(start,stop)
##    firstBatch=strIn[x[0][0]:x[0][1]]    #not sure I need x and y
##    y=findLatexWhole(r'\\question ',firstBatch, r'\\end{choices}')
##    quiz.Nquestions=len(y)#=number of questions in quiz    
##    for i in range(quiz.Nquestions):                
##        question=Question()
##        question.QA=[]#I don't know why, but code ran without this
##        question.Q=[]#ditto
##        question.A=[]#ditto
##        question.quizName=quizName
##        question.number=i+1
##        question.quizType=quizType
##        #Now we search firstBatch for the i-th QA
##        #(where QA is the "Question&Answers" string)
##        questionAndAnswer=firstBatch[y[i][0]:y[i][1]]
##        question.QA.append(questionAndAnswer)
##        [Q0,A0]=QA2QandA(questionAndAnswer)
##        question.Q.append(Q0)
##        question.A.append(A0)
##        #more needs to be added if numerical        
##        #Also need questionQ an question.A
##        quiz.questions.append(question)
##    if question.quizType=="numerical":
##        z=findLatexBetween(r'\\section{Renditions}',
##                  strIn,r'\\section{Attribution}')        
##        renditionsAll=strIn[z[0][0]:z[0][1]]
##        w=findLatexWhole(r'\\begin{questions}',renditionsAll,
##                         r'\\end{questions}')
##        for i in range(quiz.Nquestions):
##            #reopen each question from quiz
##            question=quiz.questions[i]
##            renditionsThis=renditionsAll[w[i][0]:w[i][1]]
##            v=findLatexWhole(r'\\question ',renditionsThis,
##                             r'\\end{choices}')
##            question.Nrenditions=len(v)
##            for j in range(len(v)):
##                QAj=renditionsThis[v[j][0]:v[j][1]]
##                 #########  fix pagebreak bug ################                             
##                QAj=QAj.replace('\\pagebreak',' ')
##                question.QA.append(QAj)
##                question.Q.append(QAj)
##                question.A.append(QAj)
##    return quiz #ends MakeQuiz


# -- Begin program --
allBankQuizzes=getWikiquizzes()#Updates bank




#print(allBankQuizzes)



# -- End program --




##with open(path,'w',newline='') as fout:
##    wr=csv.writer(fout,delimiter=',')
##    for row in allBankQuizzes:
##        wr.writerow([row])

##for root, dirs, files in os.walk(PATH2COURSES):
##    #print(root) print(files) not needed
##    for course in dirs:
##        print(course)

    
