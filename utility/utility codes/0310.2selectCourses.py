#import tkinter as tk
#from tkinter import (BooleanVar, Checkbutton, Button, Spinbox, Label, IntVar,
#Radiobutton)


PATH2BANK='../bank/'
PATH2COURSES='./courses/'
import os, sys, csv, re, time, copy, shutil, random#(re not used yet)
import tkinter as tk 
from tkinter import (BooleanVar, Checkbutton, Button, Spinbox, Label, IntVar,
Radiobutton, Frame)
import tkinter.ttk as ttk

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()
    def GetCourse(self):
        print("I am in GetCourse, now What?")
        self.getCourse.pack_forget()#makes button go away


    def returnQuiz(self):
        nQuiz=self.quiz1.get()#this will be a string
        NquesnQuiz=Nques[int(nQuiz)-1]
        print('Quiz=',nQuiz, ' has ',NquesnQuiz, 'questions')
        #forget ques and ques1 widgets
        self.ques.pack_forget()
        self.ques1.pack_forget()       
        #create new ques and ques1 widgets
        self.ques=Label(self) #ques for Label
        self.ques["text"]="  Ques"
        self.ques.pack(side="left")
        #set initial state for the quizzes based on Nques
        self.quesState=IntVar()
        ntemp=Nques[int(nQuiz)-1]
        self.quesState.set(ntemp) #ques1 for Spinbox
        #begin modify Spinbox in ques1 widget:
        self.ques1=Spinbox(self, width=0, wrap=1,
            from_= 1, to=Nques[int(nQuiz)-1],
            textvariable=self.quesState)
        self.ques1.pack(side="left")
        self.ques1["command"] = self.returnQues
    def returnQues(self):
        status=self.qPick.get()
        if status==True:
            OnOff="ON"
        else:
            OnOff="OFF"
        print('  Ques=',self.ques1.get(),' is ',OnOff)
                
    def returnPick(self):
        status=self.qPick.get()
        if status==True:
            OnOff="OFF"
            self.qPick.set(False)
        else:
            OnOff="ON"
            self.qPick.set(True)
        print('  Ques=',self.ques1.get(),' is ',OnOff)
                   
    def LaTeX(self):
        print("Printing exam as a LaTex .tex file")

        
            
# -- CREATING WIDGETS --           
    def create_widgets(self):
        # -- quit -- 
        self.quit = Button(self, text="QUIT", fg="red",
                              command=self.master.destroy)
        self.quit.pack(side="left")  

        # -- selectCourse --
        self.selectCourse=Label(self,text="Select course")
        self.selectCourse.pack(side="left")

        self.getCourse = ttk.Combobox(self, values=ABC)
        self.getCourse.set("testCourse")
        self.getCourse.pack(side="left")
       
        
      
        # -- latex --
        self.latex = Button(self, text="LaTeX",fg="green",
                            command=self.LaTeX)      
        self.latex.pack(side="left")

        # -- qPick pick -- (on/off button)
        self.qPick=IntVar()
        self.qPick.set(False)
        self.pick=Button(self, text="switch",
                command=self.returnPick).pack(side="right")
        # -- quiz quizState quiz1
        self.quiz=Label(self)
        self.quiz["text"]="Quiz"
        self.quiz.pack(side="left")
        self.quizState=IntVar()#Used to preset initial quiz
        self.quizState.set(Nquiz)
        self.quiz1=Spinbox(self, from_=1, to=Nquiz,
                    width=0, wrap=1, textvariable=self.quizState)
        self.quiz1.pack(side="left")
        # -- ques ques1
        self.ques=Label(self)
        self.ques["text"]=" Ques"
        self.ques.pack(side='left')
        self.ques1=Spinbox(self,from_=0, to=1, width=0)
        self.ques1.pack(side='left')
        self.quiz1["command"] = self.returnQuiz

######### -- END OF WIDGETS --       

class Question:
    def __init__(self):
        self.quizName =""
        self.number=0 #
        self.quizType = ""
        self.QA = [] #question and answer
        self.Q=[] #question only
        self.A=[] #answer only
        self.Nrenditions = 1
class Quiz:
    def __init__(self):
        self.quizName =""
        self.quizType = ""
        self.questions = []#Question() clas list
        self.Nquestions=0#number of questions
class BankInfo:  #diluted from CsvInfo in previous code
    def __init__(self):
        self.Nquestions=[]
        self.types=[]
        self.quizNames=[]
        self.Nquizzes=0
def findLatexWhole(pre,string,post):
    #returns list of indices between the pre and post
    #tags.  Whole includes the tags
    mylist=[]
    tag=pre+r'(.*?)'+post
    matches=re.finditer(tag,string,re.S)
    for item in matches:
        mylist.append([item.start(),item.end()])
    #mylist is a lists of two element lists (start/st
    #the two element list is the start/stop point in string
    #mylist is as long as there are instances of the tag
    return mylist  #ends findLatexWhole
def findLatexBetween(pre,strIn,post):
    #returns list of indeces between the pre and post
    #tags.  Between excludes the tags
    mylist=[]
    tag=pre+r'(.*?)'+post
    matches=re.finditer(tag,strIn,re.S)
    for item in matches:
    #To get "between" we subtract out the pre and post
    #part of the tag.  We need to count the backslashes
    #in order to compensate for the extra \ in the \\ escape
        n1=item.start()+len(pre)-pre.count(r'\\')
        n2=item.end()-len(post)+post.count(r'\\')#as before:
    #mylist is a lists of two element lists (start/st
    #the two element list is the start/stop point in string
    #mylist is as long as there are instances of the tag
        mylist.append([n1,n2])
    return mylist                        #ends findLatexBetween
def QA2QandA(string): 
    #Define 4 patterns:
    beginStr=r'\\begin{choices}'
    endStr=r'\\end{choices}'
    CoStr=r'\\CorrectChoice '
    chStr=r'\\choice '
    #get Q=question
    n=string.find('\\begin{choices}')
    Q=string[0:n]
    #get A=string of answers
    #newStr defines the new search parameter as 1st answer
    pat=CoStr+'|'+chStr+'|'+endStr+'(.*?)'#fails-not greedy   
    iterations=re.finditer(pat,string,re.S)
    nList=[0]
    A=[]  
    for thing in iterations:
        nList.append(thing.start())
    #for i in range(len(nList)): #crashes as expected
    for i in range(1,len(nList)-1):
        A.append(string[nList[i]:nList[i+1]])
    return [Q,A]
#def makeQuiz(path,quizName):

       
###################end functions inserted from previus######################
def getWikiquizzes():
    comment='UPGRADE rewritten at each call. Copy and rename before using.'
    bankInfo.quizNames=[] #quizname w/o .tex suffix
    bankInfo.types=[] #'conceptual' or 'numerical'
    bankInfo.Nquestions=[] #Number of questions in quiz
    bankInfo.Nquizzes=0 #Number quizzez in bank
    #get quizNames, types, and Nquestions:
    for quizFile in os.listdir(PATH2BANK): 
        if quizFile.endswith('.tex'):  #quizName
            bankInfo.Nquizzes+=1
            bankInfo.quizNames.append(quizFile[:-4])
            path2quiz=PATH2BANK+quizFile
            quizStr='' #formally called strIn
            with open(path2quiz,'r') as fin:
                for line in fin: 
                    quizStr+=line  #returns entire quiz
                tag=findLatexBetween(r'{\\quiztype}{',quizStr,'}')
                stype=quizStr[tag[0][0]:tag[0][1]]
                bankInfo.types.append(stype)
                tag=findLatexBetween(r'\\begin{questions}',  
                    quizStr, r'\\end{questions}') 
                firstBatch=quizStr[tag[0][0]:tag[0][1]]
                tag=findLatexWhole(r'\\question ',firstBatch, r'\\end{choices}')
                bankInfo.Nquestions.append(len(tag))    
                #rint(stype, len(tag), quizFile)
    print('\n',bankInfo.Nquizzes,'Wikiquizzes found in',PATH2BANK)
    path=PATH2COURSES+'allBank/UPGRADE.csv'
    fout=open(path,'w',newline='')
    writer=csv.writer(fout,delimiter=',')
    writer.writerow([comment])
    for pyCount in range(bankInfo.Nquizzes):#pyCount starts at 0
        stype=bankInfo.types[pyCount]
        if stype=="conceptual":
            tt='c'
        else:
            tt='n'
        nQuestions=bankInfo.Nquestions[pyCount]
        quizName=bankInfo.quizNames[pyCount]
        #rint(pyCount, tt, nQuestions, quizName)
        writer.writerow([str(pyCount), tt, str(nQuestions), quizName])
    fout.close()
    return bankInfo

# -- Begin program --
bankInfo=BankInfo()
bankInfo=getWikiquizzes()#Updates bank
#rint(bankInfo.quizNames)

# -- ''' BEGIN PROGRAM '''
ABC= ["A", "B", "C"]
print(bankInfo.quizNames)
Nques= 3,9,2
Nquiz=len(Nques)

##### Turn this into a widget
ABC=[]
for root, dirs, files in os.walk(PATH2COURSES):
    ss=''#root and files not needed
    for course in dirs:
        ABC.append(course)


root = tk.Tk()
Application(master=root).mainloop()







