#https://stackoverflow.com/questions/20399243
import tkinter as tk

class Example(tk.Frame):
# https://medium.com/understand-the-python/understanding-the-asterisk-of-python-8b9daaa4a558
# https://www.saltycrane.com/blog/2008/01/how-to-use-args-and-kwargs-in-python/
    def __init__(self, *args, **kwargs):
        tk.Frame.__init__(self, *args, **kwargs)
        self.l1 = tk.Label(self, text="Hover over me")
        self.l2 = tk.Label(self, text="", width=40)
        self.l1.pack(side="top")
        self.l2.pack(side="top", fill="x")

        self.l1.bind("<Enter>", self.on_enter)
        self.l1.bind("<Leave>", self.on_leave)

    def on_enter(self, event):
        self.l2.configure(text="Hello world")

    def on_leave(self, enter):
        self.l2.configure(text="")
# if __name__ ... useful of modules are calling each other		
# https://stackoverflow.com/questions/419163
if __name__ == "__main__":
    root = tk.Tk()
    Example(root).pack(side="top", fill="both", expand="true")
    root.mainloop()
