#https://docs.python.org/3.6/library/tkinter.html
import tkinter as tk
from tkinter import Tk, Button, ttk

class Gui(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.makeWidgets()

    def makeWidgets(self):
        frame=tk.Frame(self)
        frame.pack()
        bottomframe=tk.Frame(self)
        bottomframe.pack(side="bottom")
        
        self.hiThere = tk.Button(self)
        self.hiThere["text"] = "Click"
        self.hiThere["command"] = self.print2Command
        self.hiThere.pack(side="left")

        self.quit = tk.Button(self, text="QUIT", fg="red",
                              command=self.master.destroy)
        self.quit.pack(side="left")


        

##''' for next widget see:
##https://docs.python.org/3.6/library/tkinter.ttk.html#treeview
##https://docs.python.org/3.6/library/tkinter.ttk.html#item-options
##'''
        self.tree=ttk.Treeview(bottomframe)

        self.tree["columns"]=("1","2")
        self.tree.column("1", width=50 )
        self.tree.column("2", width=50)
        self.tree.heading("1", text=" A")
        self.tree.heading("2", text=" B")
        self.tree.heading("2", command=self.quit)

        id2 = self.tree.insert("", 1, "dir1", text="Dir 1")
        #self.tree.insert(id2, "end", "dir 1", text="sub dir 1", values=("1A","1B"))
        #What do I do now?
        self.tree.insert(id2, "end", "dir 1", text="sub dir 1",
                         values=("1A","1B")
                         )



        ##alternatively:
        self.tree.insert("", 3, "dir2", text="Dir 2")
        self.tree.insert("dir2", 3, text=" sub dir 2",values=("2A"," 2B"))
        self.tree.pack(side="bottom")


    def print2Command(self):
        print("Hello Command")







root = tk.Tk()
app = Gui(master=root)
app.mainloop()
