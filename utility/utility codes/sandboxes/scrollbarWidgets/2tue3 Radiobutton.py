'''
https://likegeeks.com/python-gui-examples-tkinter-tutorial/#Add-a-combobox-widget
'''
from tkinter import *
 
from tkinter.ttk import *
 
window = Tk()
 
window.title("Welcome to LikeGeeks app")
 
window.geometry('350x200')
 
rad1 = Radiobutton(window,text='0', value=1)
 
rad2 = Radiobutton(window,text='1', value=2)
 
rad3 = Radiobutton(window,text='2', value=3)
 
rad1.grid(column=0, row=0)
 
rad2.grid(column=1, row=0)
 
rad3.grid(column=2, row=0)
 
window.mainloop()

