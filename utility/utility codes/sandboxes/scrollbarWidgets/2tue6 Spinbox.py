'''
https://likegeeks.com/python-gui-examples-tkinter-tutorial/#Add-a-combobox-widget
'''


from tkinter import *
 
window = Tk()
 
window.title("Welcome to LikeGeeks app")
 
window.geometry('350x200')
 
spin = Spinbox(window, from_=0, to=2, width=2)
 
spin.grid(column=0,row=0)
 
window.mainloop()
