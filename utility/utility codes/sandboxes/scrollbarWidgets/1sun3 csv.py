'''
https://stackoverflow.com/questions/18339893/using-tkinter-to-export-user-input-to-a-csv-file
Modified to solve some problems with appending and lineterminator
'''
from tkinter import * 
import csv

class App(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.pack()
        self.output()

    def output(self):
        Label(text='Name:').pack(side=LEFT,padx=5,pady=5)
        self.e = Entry(root, width=50)
        self.e.pack(side=LEFT,padx=5,pady=5)

        self.b = Button(root, text='Submit', command=self.writeToFile)
        self.b.pack(side=RIGHT,padx=5,pady=5)

    def writeToFile(self):
        with open('1sun3.csv', 'a') as f:
            w=csv.writer(f,lineterminator = '\n')#, quoting=csv.QUOTE_ALL)
            w.writerow([self.e.get()])

#This clears out old versions so we can later append
f=open('1sun3.csv', 'w')
f.close()            
    

root=Tk()
root.title('Auto Logger')
root.geometry('1000x100')
app=App(master=root)
app.mainloop()
root.mainloop()
