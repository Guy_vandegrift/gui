'''https://stackoverflow.com/questions/32511843'''
from tkinter import Tk, Button, ttk

root = Tk()

tree = ttk.Treeview(root)

tree["columns"]=("1","2")
tree.column("1", width=50 )
tree.column("2", width=50)
tree.heading("1", text=" A")
tree.heading("2", text=" B")

id2 = tree.insert("", 1, "dir1", text="Dir 1")
tree.insert(id2, "end", "dir 1", text="sub dir 1", values=("1A","1B"))

##alternatively:
tree.insert("", 3, "dir2", text="Dir 2")
tree.insert("dir2", 3, text=" sub dir 2",values=("2A"," 2B"))



tree.pack()


root.mainloop()
