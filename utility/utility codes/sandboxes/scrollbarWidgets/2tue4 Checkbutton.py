'''
https://likegeeks.com/python-gui-examples-tkinter-tutorial/#Add-a-combobox-widget
'''

from tkinter import *
 
from tkinter.ttk import *
 
window = Tk()
 
window.title("Welcome to LikeGeeks app")
 
window.geometry('350x200')
 
chk_state1 = BooleanVar()
chk_state2 = BooleanVar()
 
chk_state1.set(True) #set check state
chk_state2.set(True) #set check state
 
chk1 = Checkbutton(window, text='1', var=chk_state1)
chk2 = Checkbutton(window, text='2', var=chk_state2)
 
chk1.grid(column=0, row=0)
chk2.grid(column=1, row=0)
 
window.mainloop()
