#import tkinter as tk
#from tkinter import (BooleanVar, Checkbutton, Button, Spinbox, Label, IntVar,
#Radiobutton)

import os, sys, csv, re, time, copy, shutil, random#(re not used yet)
PATH2BANK='../bank/'
PATH2COURSES='./courses/'
class Question:
    def __init__(self):
        self.quizName =""
        self.number=0 #
        self.quizType = ""
        self.QA = [] #question and answer
        self.Q=[] #question only
        self.A=[] #answer only
        self.Nrenditions = 1
class Quiz:
    def __init__(self):
        self.quizName =""
        self.quizType = ""
        self.questions = []#Question() clas list
        self.Nquestions=0#number of questions
class BankInfo:  #diluted from CsvInfo in previous code
    def __init__(self):
        self.Nquestions=[]
        self.types=[]
        self.quizNames=[]
        self.Nquizzes=0
def findLatexWhole(pre,string,post):
    #returns list of indices between the pre and post
    #tags.  Whole includes the tags
    mylist=[]
    tag=pre+r'(.*?)'+post
    matches=re.finditer(tag,string,re.S)
    for item in matches:
        mylist.append([item.start(),item.end()])
    #mylist is a lists of two element lists (start/st
    #the two element list is the start/stop point in string
    #mylist is as long as there are instances of the tag
    return mylist  #ends findLatexWhole
def findLatexBetween(pre,strIn,post):
    #returns list of indeces between the pre and post
    #tags.  Between excludes the tags
    mylist=[]
    tag=pre+r'(.*?)'+post
    matches=re.finditer(tag,strIn,re.S)
    for item in matches:
    #To get "between" we subtract out the pre and post
    #part of the tag.  We need to count the backslashes
    #in order to compensate for the extra \ in the \\ escape
        n1=item.start()+len(pre)-pre.count(r'\\')
        n2=item.end()-len(post)+post.count(r'\\')#as before:
    #mylist is a lists of two element lists (start/st
    #the two element list is the start/stop point in string
    #mylist is as long as there are instances of the tag
        mylist.append([n1,n2])
    return mylist                        #ends findLatexBetween
def QA2QandA(string): 
    #Define 4 patterns:
    beginStr=r'\\begin{choices}'
    endStr=r'\\end{choices}'
    CoStr=r'\\CorrectChoice '
    chStr=r'\\choice '
    #get Q=question
    n=string.find('\\begin{choices}')
    Q=string[0:n]
    #get A=string of answers
    #newStr defines the new search parameter as 1st answer
    pat=CoStr+'|'+chStr+'|'+endStr+'(.*?)'#fails-not greedy   
    iterations=re.finditer(pat,string,re.S)
    nList=[0]
    A=[]  
    for thing in iterations:
        nList.append(thing.start())
    #for i in range(len(nList)): #crashes as expected
    for i in range(1,len(nList)-1):
        A.append(string[nList[i]:nList[i+1]])
    return [Q,A]
#def makeQuiz(path,quizName):

       
###################end functions inserted from previus######################
def getWikiquizzes():
    comment='UPGRADE rewritten at each call. Copy and rename before using.'
    bankInfo.quizNames=[] #quizname w/o .tex suffix
    bankInfo.types=[] #'conceptual' or 'numerical'
    bankInfo.Nquestions=[] #Number of questions in quiz
    bankInfo.Nquizzes=0 #Number quizzez in bank
    #get quizNames, types, and Nquestions:
    for quizFile in os.listdir(PATH2BANK): 
        if quizFile.endswith('.tex'):  #quizName
            bankInfo.Nquizzes+=1
            bankInfo.quizNames.append(quizFile[:-4])
            path2quiz=PATH2BANK+quizFile
            quizStr='' #formally called strIn
            with open(path2quiz,'r') as fin:
                for line in fin: 
                    quizStr+=line  #returns entire quiz
                tag=findLatexBetween(r'{\\quiztype}{',quizStr,'}')
                stype=quizStr[tag[0][0]:tag[0][1]]
                bankInfo.types.append(stype)
                tag=findLatexBetween(r'\\begin{questions}',  
                    quizStr, r'\\end{questions}') 
                firstBatch=quizStr[tag[0][0]:tag[0][1]]
                tag=findLatexWhole(r'\\question ',firstBatch, r'\\end{choices}')
                bankInfo.Nquestions.append(len(tag))    
                #rint(stype, len(tag), quizFile)
    print('\n',bankInfo.Nquizzes,'Wikiquizzes found in',PATH2BANK)
    path=PATH2COURSES+'allBank/UPGRADE.csv'
    fout=open(path,'w',newline='')
    writer=csv.writer(fout,delimiter=',')
    writer.writerow([comment])
    for pyCount in range(bankInfo.Nquizzes):#pyCount starts at 0
        stype=bankInfo.types[pyCount]
        if stype=="conceptual":
            tt='c'
        else:
            tt='n'
        nQuestions=bankInfo.Nquestions[pyCount]
        quizName=bankInfo.quizNames[pyCount]
        #rint(pyCount, tt, nQuestions, quizName)
        writer.writerow([str(pyCount), tt, str(nQuestions), quizName])
    fout.close()
    return bankInfo

# -- Begin program --
bankInfo=BankInfo()
bankInfo=getWikiquizzes()#Updates bank
#rint(bankInfo.quizNames)

for root, dirs, files in os.walk(PATH2COURSES):
    ss=''
    for course in dirs:
        ss+=course+' '
    print(ss)



