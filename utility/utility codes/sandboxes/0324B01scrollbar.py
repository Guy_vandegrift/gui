''' ttk_Scrollbar101.py
explore the ttk scrollbar widget
https://www.daniweb.com/programming/software-development/threads/492872
/scrollbar-not-working
'''
import tkinter as tk
import tkinter.ttk as ttk
root = tk.Tk()
scrollbar = ttk.Scrollbar(root)
scrollbar.pack(side='right', fill='y')
listbox = tk.Listbox(root, yscrollcommand=scrollbar.set)
listbox = tk.Listbox(root, xscrollcommand=scrollbar.set)
# load the listbox
for n in range(1, 26):
   listbox.insert('end', "This is line number " + str(n))
listbox.pack(side='left', fill='both')
scrollbar.config(command=listbox.yview)
root.mainloop()
