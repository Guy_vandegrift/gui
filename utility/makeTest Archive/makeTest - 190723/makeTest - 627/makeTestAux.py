##!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
##!  !DEBUGComment!  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
##!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

import os, sys, csv, re, time, copy, shutil, random
                        
class Question:                                          
    def __init__(self):
        self.quizName =""
        self.number=0 #
        self.quizType = ""
        self.QA = [] #question and answer
        self.Q=[] #question only
        self.A=[] #answer only
        self.Nrenditions = 1
        self.imageList=[]
class Quiz:
    def __init__(self):
        self.quizName =""
        self.quizType = ""
        self.questions = []#Question() clas list
        self.Nquestions=0#number of questions


class CsvInfo:  #formerly CsvInfo
    def __init__(self):
        self.comments=[]#formerly col 1a       
        self.quizNames=[]#formerly col 2b
        self.selections=[]#formerly col 3c (Sel)
        self.Nquestions=[]#formerly col 4d (available)
        self.types=[]# t,n,c formerlycol 4e 
        self.choices=[] #list formerly starts at 5f


def whatis(string, x):
    print(string+' value=',repr(x),type(x))
    return string+' value='+repr(x)+repr(type(x))

def getCourse():    #Gets courseName and quizList from a textfile
    availableCourses=[]
    for item in os.listdir('./input'):
        if item.endswith('.txt'):
            availableCourses.append(item[:-4])
    for i in range(len(availableCourses)):
        print(i+1,availableCourses[i])
    if len(availableCourses)==1:
        courseName=availableCourses[0]
    else:
        n=input("Enter integer for course: ")
        courseName=availableCourses[int(n)-1]

    path2course='./input/'+courseName+'.txt'
    lines = open(path2course).read().splitlines()
    #strip whitespaces
    for i in range(len(lines)):
        lines[i]=lines[i].strip()
    #remove empty lines
    quizList = list(filter(None, lines))
    return [courseName, quizList]

def findLatexWhole(pre,string,post):
    #returns list of indices between the pre and post
    #tags.  Whole includes the tags
    mylist=[]
    tag=pre+r'(.*?)'+post
    matches=re.finditer(tag,string,re.S)
    for item in matches:
        mylist.append([item.start(),item.end()])
    #mylist is a lists of two element lists (start/st
    #the two element list is the start/stop point in string
    #mylist is as long as there are instances of the tag
    return mylist  #ends findLatexWhole
def findLatexBetween(pre,strIn,post):
    #returns list of indeces between the pre and post
    #tags.  Between excludes the tags
    mylist=[]
    tag=pre+r'(.*?)'+post
    matches=re.finditer(tag,strIn,re.S)
    for item in matches:
    #To get "between" we subtract out the pre and post
    #part of the tag.  We need to count the backslashes
    #in order to compensate for the extra \ in the \\ escape
        n1=item.start()+len(pre)-pre.count(r'\\')
        n2=item.end()-len(post)+post.count(r'\\')#as before:
    #mylist is a lists of two element lists (start/st
    #the two element list is the start/stop point in string
    #mylist is as long as there are instances of the tag
        mylist.append([n1,n2])
    return mylist                        #ends findLatexBetween

def QA2QandA(string): 
    #Define 4 patterns:
    beginStr=r'\\begin{choices}'
    endStr=r'\\end{choices}'
    CoStr=r'\\CorrectChoice '
    chStr=r'\\choice '
    #get Q=question
    n=string.find('\\begin{choices}')
    Q=string[0:n]
    #get A=string of answers
    #newStr defines the new search parameter as 1st answer
    pat=CoStr+'|'+chStr+'|'+endStr+'(.*?)'#fails-not greedy   
    iterations=re.finditer(pat,string,re.S)
    nList=[0]
    A=[]  
    for thing in iterations:
        nList.append(thing.start())
    #for i in range(len(nList)): #crashes as expected
    for i in range(1,len(nList)-1):
        A.append(string[nList[i]:nList[i+1]])
    return [Q,A]

def makeQuiz(path,quizName):
    strIn=""
    with open(path,'r') as fin:
      for line in fin:
        strIn+=line
        ########### we build Quiz() etc from strIn
    quiz=Quiz()
    quiz.quizName=quizName  
    x=findLatexBetween(r'{\\quiztype}{',strIn,'}')
    quizType=strIn[x[0][0]:x[0][1]]
    quiz.quizType=quizType                      
    x=findLatexBetween(r'\\begin{questions}',  #List of index pairs
                    strIn,r'\\end{questions}') #(start,stop)
    #julyadd firstBatch contains all the questions
    firstBatch=strIn[x[0][0]:x[0][1]]    #not sure I need x and y    
    y=findLatexWhole(r'\\question',firstBatch, r'\\end{choices}')
    quiz.Nquestions=len(y)#=number of questions in quiz
    for i in range(quiz.Nquestions):                
        question=Question()
        question.QA=[]#I don't know why, but code ran without this
        question.Q=[]#ditto
        question.A=[]#ditto
        question.imageList=[]#julyadd 
        question.quizName=quizName
        question.number=i+1
        question.quizType=quizType
        #Now we search firstBatch for the i-th QA
        #(where QA is the "Question&Answers" string)
        questionAndAnswer=firstBatch[y[i][0]:y[i][1]]
        question.QA.append(questionAndAnswer)
        [Q0,A0]=QA2QandA(questionAndAnswer)
        question.Q.append(Q0)
        question.A.append(A0)

        #Find images for each question:
        #Since I used x and y to find the questions, zIm will find images
        #zIm is a list of 2-element lists that mark strings of form *.png
        zIm=findLatexBetween(r'{images/',
                questionAndAnswer, r'}')
        question.imageList=[]        
        if zIm!=[]:  #if the image list is not empty
            for ic in range(len(zIm)): #ic counts imates
                print('imageCount',ic)
                imageBatch=questionAndAnswer[zIm[ic][0]:zIm[ic][1]]
                question.imageList.append(imageBatch)
        else:    #insert alligator if there are no images
            imageBatch="lake-gator.png"
            question.imageList.append(imageBatch)
        quiz.questions.append(question)
    if question.quizType=="numerical":
        z=findLatexBetween(r'\\section{Renditions}',
                  strIn,r'\\section{Attribution}')        
        renditionsAll=strIn[z[0][0]:z[0][1]]
        w=findLatexWhole(r'\\begin{questions}',renditionsAll,
                         r'\\end{questions}')
        for i in range(quiz.Nquestions):
            #reopen each question from quiz
            question=quiz.questions[i]
            renditionsThis=renditionsAll[w[i][0]:w[i][1]]
            v=findLatexWhole(r'\\question',renditionsThis,
                             r'\\end{choices}')
            question.Nrenditions=len(v)
            for j in range(len(v)):
                QAj=renditionsThis[v[j][0]:v[j][1]]
                 #########  fix pagebreak bug ################                             
                QAj=QAj.replace('\\pagebreak',' ')
                question.QA.append(QAj)
                question.Q.append(QAj)
                question.A.append(QAj)                
    return quiz #ends MakeQuiz

def cleanFragment(fragment, lengthOfPreview):
    fragment=fragment[10:]
    fragment=fragment.replace('\n','')
    fragment=fragment.replace('\\newline ',' ')
    fragment=re.sub(' +', ' ',fragment)
    n1=fragment.find('\includegraphics')   
    if n1>0:
        n2=fragment.find('.png')
        imageStuff=fragment[n1:n2+5]
        fragment=fragment.replace(imageStuff,'')
        fragment=fragment[:lengthOfPreview]
    else:
        fragment=fragment[:lengthOfPreview]
    n3=fragment.find('ifkey')
    if n3>0:
        fragment=fragment[:n3-1]
    return fragment

def mainWindowSize(MAINX,MAINY):
    return str(MAINX)+"x"+str(MAINY)

def modifyQuote(quote):
    #modifys the quiz.Q for printing into the text widget
    quote=quote.replace('\n', ' ')
    quote=re.sub(' +', ' ',quote)
    matches=findLatexWhole(r'\\ifkey',quote,r'\\fi')
    if matches:
        n1=matches[0][0]
        n2=matches[0][1]
        quote=quote[:n1]+quote[n2:]   
    return quote  
#I think functions that call widget variables must be defined later(?)







