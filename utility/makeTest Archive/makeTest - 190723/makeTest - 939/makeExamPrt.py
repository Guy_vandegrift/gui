
#import os, sys, csv, re, time, copy, shutil, random#(re not used yet)
import time
global csvInfo
##from makeTestAux import whatis, getCourse, makeQuiz,\
##    findLatexWhole, findLatexBetween, QA2QandA, Question,\
##    Quiz, CsvInfo, cleanFragment, mainWindowSize, \
##    modifyQuote

from makeTestAux import whatis, getCourse, makeQuiz,\
    findLatexWhole, findLatexBetween, QA2QandA, Question,\
    Quiz, CsvInfo, cleanFragment, mainWindowSize, \
    modifyQuote

    
def get_statement():
    statement='%statement\n'
    statement+='Though posted on Wikiversity, this document was created without '
    statement+='wikitex using Python to write LaTeX markup.  With a bit more development '
    statement+='it will be possible for users to download and use software that will '
    statement+='permit them to create, modify, and print their own versions of this document.\n'
    statement+='% insert more here...\\\\\n'
    return statement

def getFromChoices(first,second,num2pick): #select num2pick items
    #I am not sure I need this.  I am not overselecting any more
    random.shuffle(first)#list of integers first priority
    random.shuffle(second)#list of integers second priority
    allofem=(first+second)
    Nmax=min(len(allofem),num2pick)#Don't pick more than available
    selected=allofem[0:Nmax]
    random.shuffle(selected)
    return selected



def prefixUnderscore(string):
    #needed because Latex does not recobnize the underscore
    string=string.replace('_','\\_')
    return string    

def startLatex(titleTxt,quizType,statement,timeStamp): #this starts the Latex markup
    titleTex=titleTxt.replace('_','\\_')
    string=r'''%PREAMBLE
\newcommand{\quiztype}{'''+quizType+r'''}
\newif\ifkey\documentclass[11pt,twoside]{exam}
\RequirePackage{amssymb, amsfonts, amsmath, latexsym, verbatim,
xspace, setspace,datetime,tikz, pgflibraryplotmarks, hyperref,
textcomp}
\usepackage[left=.4in, right=.4in, bottom=.9in, top=.7in]{geometry}
\usepackage{endnotes, multicol,textgreek,graphicx} \singlespacing 
\parindent 0ex \hypersetup{ colorlinks=true, urlcolor=blue}
\pagestyle{headandfoot}
\runningheader{'''+titleTex+r'''}{\thepage\ of \numpages}{'''\
+timeStamp+r'''}
\footer{}
{\LARGE{The next page might contain more answer choices for this question}}{}
% BEGIN DOCUMENT 
\begin{document}\title{'''+titleTex+r'''}
\author{\includegraphics[width=0.10\textwidth]
{images/666px-Wikiversity-logo-en.png}\\
The LaTex code that creates this quiz is released to the Public Domain\\
Attribution for each question is documented in the Appendix\\
\url{https://bitbucket.org/Guy_vandegrift/qbwiki/wiki/Home}\\
\url{https://en.wikiversity.org/wiki/Quizbank} \\
'''+quizType+r''' quiz '''+timeStamp+\
r'''}
\maketitle
'''+statement+"\n\\tableofcontents\n"
    return string
def AllStudyLatex(testName,statement,timeStamp,bnkQuizzes):
    bigStrAll=startLatex(testName+": All","mixed",statement,timeStamp)
    bigStrStudy=startLatex(testName+":Study","mixed",statement,timeStamp)
    NbnkQuiz=len(bnkQuizzes)
    for i in range(NbnkQuiz):
        quiz=bnkQuizzes[i]
        quizNameLatex=prefixUnderscore(quiz.quizName)    
        bigStrAll+=r'\section{'+quizNameLatex\
            +r'}\keytrue\printanswers'
        bigStrStudy+=r'\section{'+quizNameLatex\
            +r'}\keytrue\printanswers'
        #print zeroth renditions
        bigStrAll+='\n\\begin{questions}\n'
        bigStrStudy+='\n\\begin{questions}\n' 
        for j in range(quiz.Nquestions):
            thisQA=quiz.questions[j].QA
            bigStrAll+=thisQA[0]
            bigStrStudy+=thisQA[0]
        bigStrAll+='\n\\end{questions}\n'
        bigStrStudy+='\n\\end{questions}\n'
        if(quiz.questions[j].quizType)=='numerical':
            bigStrAll+='\n \\subsection{Renditions}'     
            for j in range(quiz.Nquestions): #iterates questions not renditions 
                bigStrAll+='\n\\subsubsection*{'+quizNameLatex+' Q'+str(j+1)+'}'
                thisQA=quiz.questions[j].QA
                bigStrAll+='\n\\begin{questions}\n'            
                for k in range(1,len(thisQA)): #A null loop if conceptual
                    bigStrAll+=thisQA[k]
                bigStrAll+='\n\\end{questions}\n'
    bigStrAll+='\n\\section{Attribution}\\theendnotes\n\\end{document}'
    bigStrStudy+='\n\\section{Attribution}\\theendnotes\n\\end{document}'
    #Make output
    path2=os.path.join('.\output',timeName)
    if not os.path.exists(path2):
        os.makedirs(path2)
    pathAll=os.path.join('.\output',timeName+'All.tex')
    with open(pathAll,'w') as latexOut:
        latexOut.write(bigStrAll)
    pathStudy=os.path.join('.\output',timeName+'Study.tex')
    with open(pathStudy,'w') as latexOut:
        latexOut.write(bigStrStudy)
    return
def checkcsvData(csvData, bnkQuizzes):
    csvChecks=True
    csvNew=copy.deepcopy(csvData)
    NquestionsMax=0 #will grow to the most questions on any wikiquiz
    NquestionsOnTest=0 #This will sum up the selections column 3c
    NquestionsInBnk=0#number questions in (local) bank
    #skip the top row of csvDat for now:
    Nquizzes=len(bnkQuizzes)
    for i in range(Nquizzes):       
        #col 1a comments. No need to check (except for top row).
        #col 2b testname
        thing=bnkQuizzes[i].quizName
        ss=csvData[i+1][1]   
        if repr(thing)!=repr(ss):
            print(thing+', '+ss+',quizname mismatch fatal error')
            sys.exit()
            csvChecks=False
        #col 3c Sel=number selected for test. No need to check.
        NquestionsOnTest+=int(csvData[i+1][2])
        #col 4d number questions available from bank
        thing=bnkQuizzes[i].Nquestions
        NquestionsInBnk+=thing
        if thing>NquestionsMax:
            NquestionsMax=thing
        ss=csvData[i+1][3]
        if str(thing)!=str(ss):
            print('thing', repr(thing), 'ss', repr(ss))
            print(str(thing),ss,'available questions mismatch')
            csvNew[i+1][3]=thing
            csvChecks=False
        #col 5e t=type
        thing=bnkQuizzes[i].quizType
        ss=csvData[i+1][4]
        if thing=='conceptual':
            thing='c'
        if thing=='numerical':
            thing='n'
        if thing!=ss:
            print(thing,ss,'quiztype mismatch')
            csvNew[i+1][4]=str(thing)
            csvChecks=False
    #col 6f  1 (2,3,...) were obtained from the ss=spreadsheet
    #Now do the first row:  Now thing has to be calculated
    #col 1a thing is the number of questions on the Test
    ss=csvData[0][0] #col 1a has index 0
    thing=NquestionsOnTest
    if str(thing)!=str(ss):
        print(repr(thing),repr(ss),'Questions on Test mismatch')
        csvNew[0][0]=str(thing)
        csvChecks=False
    #ss=csvData[0][1] #col 2b is testName index 1
    ss=testName #here we disable the testName check
    thing=testName
    if str(thing)!=str(ss):
        print(thing,ss,'TestName mismatch')
        csvNew[0][1]=thing
        csvChecks=False
    ss=csvData[0][2] #col 3c Sel selection label index 2
    thing='Sel'
    if str(thing)!=str(ss):
        print(thing,ss,'Sel=selection col header not convention')
        csvNew[0][2]=thing
        csvChecks=False
    ss=csvData[0][3]
    thing=str(NquestionsInBnk)
    if str(thing)!=str(ss):
        print(thing,ss,'questions in (local) bank mismatch')
        csvNew[0][3]=str(thing)
        csvChecks=False
    ss=csvData[0][4] #col 5e is 't' type
    thing='t'
    if str(thing)!=str(ss):
        print(thing,ss,"needs 't' in top of column")
        csvNew[0][4]=thing
        csvChecks=False
    ss=csvData[0][5:len(csvData[0])]
    problemLabels=map(str,list(range(1,NquestionsMax+1)))
    thing=list(problemLabels)
    if str(thing)!=repr(ss)and str(thing)!=repr(ss[:-1]):
        print(thing,ss,'problemLabels mismatch')
        for n in range(0,NquestionsMax):
            csvNew[0][5+n]=str(n+1)
        csvChecks=False
    print('csvChecks =',csvChecks)
##    if csvChecks:
##        print('csv file seems correct')
##    else:
##        print('csv file needs UPGRADE - upgrading now')
##        upgradePath="./input/"+testName+'UPGRADE.csv'
##        with open(upgradePath,'w',newline='') as fout:
##            wr=csv.writer(fout,delimiter=',')
##            for row in csvNew:
##                wr.writerow(row)
    return csvChecks
def instructorLatex(testName,statement,timeStamp,bnkQuizzes,csvInfo):    
    bigStr=startLatex(testName+": Instructor","mixed",statement,timeStamp)
    bigStr+=r'\keytrue\printanswers'
    choiceList=[]
    for i in range(len(bnkQuizzes)):
        bigStr+=r'%New bank wikiquiz\\'
        bigStr+='\n'
        quiz=bnkQuizzes[i]
        quizName=quiz.quizName
        #must upgrade quizName to Latex format:
        quizName=quizName.replace('_','\\_')
        Nquestions=quiz.Nquestions        
        num2pick_str=csvInfo.selections[i+1]
        bigStr+=r'\subsection{'+num2pick_str
        bigStr+=' of '+ str(Nquestions) +' questions from '+quizName+'}\n'
        first=[]
        second=[]
        #choiceSel is the decision made regarding each question
        #in the wikiquiz: 0, 1, 2, or if outide range:''
        choiceSel=csvInfo.choices[i+1]
        #convert this list of "integer strings" to a string:
        bigStr+='\nSel: '
        bigStr+=', '.join(choiceSel)+r'.\\'
        iMax=len(choiceSel)
        for j in range(iMax):#note that the list starts at 1 not 0
            if choiceSel[j]=='1':
                first.append(int(j+1))
            if choiceSel[j]=='2':
                second.append(int(j+1))
        #randChoice selects randomly from this:
        randChoice=getFromChoices(first,second,int(num2pick_str))
        randChoice=sorted(randChoice)
        #create a printable string to show the random selection:
        bigStr+='\nRandom: '
        bigStr+=', '.join(map(str,randChoice))+r'.\\'+'\n'
        #choiceList is a list of lists:
        choiceList.append(randChoice)
        #all versions use these same choices and choice list saves
        #Review: i indexed the quizzes, so we let j index the questions

        #Begin fix to avoid void question gag
        if len(randChoice)!=0:
            bigStr+='\n'+'\\begin{questions}\n'
            for j in range(len(randChoice)):            
                jQ=randChoice[j]
                #print('randomChoice=jQ',jQ,'bnkquiz#=i',i)
                #recall that humans count questions beginning at 1
                #but python starts at zero:  jQ goes to jQ-1
                bigStr+=quiz.questions[jQ-1].QA[0]
            bigStr+='\n\\end{questions}\n'
        #End fix to avoid void gquestion tag
        
    bigStr+='\n\\section{Attribution}\n'
    bigStr+='Some attributions are located elsewhere.'
    bigStr+=r'''\endnote{The current system neglects to repeat the
attributions for the multipe renditions}'''
    bigStr+='\n\\theendnotes\n\\end{document}'        
    pathInstructor=os.path.join('.\output',timeName+'Instructor.tex')
    with open(pathInstructor,'w') as latexOut:
        latexOut.write(bigStr)
    return choiceList
def choiceList2string(choiceList,bnkQuizzes): #wherewasi
    #returns random selections from the choiceList in randomized order
    testQAlist=[]
    for i in range(len(bnkQuizzes)):
        quiz_i=bnkQuizzes[i]
        for j in range(len(choiceList[i])):
            choice_j=choiceList[i][j]
            jPy=choice_j-1#Python iterates from zero
            #choice_j is an integer
            question=quiz_i.questions[jPy]
            if question.quizType=="numerical":
                NR=question.Nrenditions
                nR=random.choice(range(1,NR))
                testQAlist.append(question.QA[nR])
            if question.quizType=="conceptual":
                Q=question.Q[0]
                Araw=question.A[0]
                Ashuffled=question.A[0]
                random.shuffle(Ashuffled)
                string=Q+'\\begin{choices}'
                for answer in Ashuffled:
                    string+=answer
                string+='\\end{choices}\n'
                #testQAlist.append(Q+Ashuffled)
                testQAlist.append(string)    
    random.shuffle(testQAlist)
    bigStr=''
    for item in testQAlist:
        bigStr+=item+'\n'
    return bigStr

def testLatex(testName,statement,timeStamp,bnkQuizzes,choiceList,
              nVersions):
    questionList=[]
    for n in range(len(bnkQuizzes)):
        quiz=bnkQuizzes[n]
        string=quiz.quizName+' ('+str(quiz.Nquestions)
        string+=' '+quiz.quizType+') choices: '
        string+=', '.join(map(str,choiceList[n]))
        #print(string)
        for quesNumStr in choiceList[n]:
            #pyQuesIndex gets us the question we want:
            pyQuesIndex=int(quesNumStr)-1
            question=quiz.questions[pyQuesIndex]
            questionList.append(question)
    bigStr=startLatex(testName+": Test","mixed",statement,timeStamp)
    #startLatex fixes the prefixUnderscore, but henceforth we need:
    testNameLatex=prefixUnderscore(testName)
    bigStr+='text before first subsection\n'
    for nver in range(nVersions):
        #noanswers part:
        bigStr+='\\cleardoublepage\\subsection{V'+str(nver+1)+'}\n'
        bigStr+='\\noprintanswers\\keyfalse\n'
        bigStr+='\\begin{questions}\n'
        testQAlist= choiceList2string(choiceList,bnkQuizzes)
        bigStr+=testQAlist       
        bigStr+='\\end{questions}\n'

        ##KEY part
        bigStr+='\\cleardoublepage\\subsubsection{KEY V'+str(nver+1)+'}\n'
        bigStr+='\\printanswers\\keyfalse\n'
        bigStr+='\\begin{questions}\n'
        bigStr+=testQAlist         
        bigStr+='\\end{questions}\n'    
    bigStr+='\n\\section{Attribution}\n'
    bigStr+='Some attributions are located elsewhere.'
    bigStr+=r'''\endnote{The current system neglects to repeat the
attributions for the multipe renditions}'''
    bigStr+='\n\\theendnotes\n\\end{document}'  
    pathTest=os.path.join('.\output',timeName+'Test.tex')
    with open(pathTest,'w') as latexOut:
        latexOut.write(bigStr)
    return 
    
    
####################   Original program started here (now its a function) ##########################
def print2output(csvInfo):


    timeStamp=str(int(time.time()*100))
    Nversions=int(input("Nversions: "))
    #rint(Nversions,timeStamp)
    whatis('csvInfo',csvInfo)

##    csvRows=len(csvInfo.quizNames)
##    bnkQuizNames=csvInfo.quizNames[1:csvRows]

    #AllStudyLatex(testName,statement,timeStamp,bnkQuizzes)


    #csvInfo,cvsRows,csvData]=getCsv(csvInfo,testName)
    #bnkQuizNames is a list of quznames in the bank
    
    
    # csvRows=len(csvInfo.quizNames)
    # bnkQuizNames=csvInfo.quizNames[1:cvsRows]
    # bnkQuizzes=[]#bnkQuizzes is a list of objects of class Quiz()
    # for name in bnkQuizNames:
        # if os. path. isfile('./bank/'+name+'.tex'):
            # bnkQuizzes.append(makeQuiz('./bank/'+name+'.tex',name))
        # else:
            # print(name+'is not a quiz in the bank')
    # csvChecks=checkcsvData(csvData, bnkQuizzes)
    
    # AllStudyLatex(testName,statement,timeStamp,bnkQuizzes)

    
    # choiceList=instructorLatex(testName,statement,timeStamp,bnkQuizzes,csvInfo)
    # testLatex(testName,statement,timeStamp,bnkQuizzes,choiceList,
              # Nversions)

    



