
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!!!!!! DEBUG !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#Unfortuantely I had to define global variables that are not fixed:
global TESTNUMBERCOUNT, SELECTEDQUIZ,MAINX,MAINY,PREVX, PREVY,WRAPLENGTH
#the following global variables are fixed:
global csvInfo,quizList,courseName,lengthOfPreview, fontSize,\
        NtestCol, quizNameCol, preCol,inQuizCol,quesCol,bigCol
#csvInfo contains instructions for selecting questions, the questions
#are obtained by makeQuiz to retrieve each quiz in the bank

import os, sys, csv, re, time, copy, shutil, random#(re not used yet)
import tkinter as tk
from functools import partial #allows calling with parameter
from PIL import ImageTk,Image #needed for my png images

#The following functions were created here and moved to makeTestAuz
from makeTestAux import whatis, getCourse, makeQuiz,\
    findLatexWhole, findLatexBetween, QA2QandA, Question,\
    Quiz, CsvInfo, cleanFragment, mainWindowSize, \
    modifyQuote

csvInfo=CsvInfo()

#SET WINDOW SIZES FOR MAIN GRID AND QUESTION PREVIEW:
MAINX,MAINY,PREVX,PREVY=980, 500, 60, 35
WRAPLENGTH=400
fontSize=11#sets font size for the GUI grid, not for the exams
lengthOfPreview=150#terminates question length in quizCol
#I gave the columns names in case this gets redesigned:
(NtestCol,quizNameCol,preCol,imageCol,inQuizCol,quesCol)=(0,1,3,10,11,12)
#   0          1         3      10,      11        12




# I plan to move these functions into AUX


def pickQuizColor(iQ):
    global SELECTEDQUIZ
    if SELECTEDQUIZ==iQ:
        color="#D3D3D3"
    else:
        color="white"
    return color


class Application(tk.Frame):  
    global TESTNUMBERCOUNT, SELECTEDQUIZ
    def __init__(self, root):
        # I don't really understand this stuff:
        tk.Frame.__init__(self, root)
        self.canvas = tk.Canvas(root, borderwidth=2,
                        relief="groove", background="#ffffff")
        self.frame = tk.Frame(self.canvas, background="#ffffff")
        self.vsb = tk.Scrollbar(root, orient="vertical",
                                command=self.canvas.yview)
        self.hsb = tk.Scrollbar(root, orient="horizontal",
                                command=self.canvas.xview)
        self.canvas.configure(yscrollcommand=self.vsb.set)
        self.canvas.configure(xscrollcommand=self.hsb.set)
        self.vsb.pack(side="right", fill="y")
        self.hsb.pack(side="bottom", fill="y")
        self.canvas.pack(side="left", fill="both", expand=True)
        self.canvas.create_window((4,4),window=self.frame,
          anchor="nw", tags="self.frame")#(*,*) was (4,4)
        self.frame.bind("<Configure>", self.onFrameConfigure)     
    def onFrameConfigure(self, event): 
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))
        self.firstWidgets()

## This destroys the prview box and image
    def alsoDestroy(self):
        for item in self.frame.grid_slaves():
            if int(item.grid_info()["column"])==preCol:
                item.destroy()  #grid_forget() also works
        #For some reason I need two seperate loops, or this crashes:
        for item in self.frame.grid_slaves():              
            if int(item.grid_info()["column"])==imageCol:
                item.destroy()  #grid_forget() also works
        ## see also "if int(item.grid_info()["column"])==imageCol"
                        
               
    def questionPreview(self,iQ,iq,showPreview): 
    #WIDGET questionPreview !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        for item in self.frame.grid_slaves():
            if int(item.grid_info()["column"])==preCol:
                   item.destroy()  #grid_forget() also works                    
        quiz = makeQuiz('./bank/'+quizList[iQ]+'.tex',quizList[iQ])
        #here we get the questionQA for the preview  
        quote=quiz.questions[iq].QA[0]
        quote=modifyQuote(quote)
        headertext=str(iQ+1)+' '+quizList[iQ]+": "
        headertext+="Question "+str(iq+1)+"\n"
        quote=headertext+quote
        questionPreview=tk.Label(self.frame,
            width=PREVX,borderwidth=4,text=quote,
            wraplength=WRAPLENGTH,
            anchor="nw")      
        questionPreview.grid(row=1+iq, column=preCol)
        #destroy previous images in imageCol
        for item in self.frame.grid_slaves():
            if int(item.grid_info()["column"])==imageCol:
                item.destroy()  #grid_forget() also works
                
        #images4question is the list of images for question #iq
        #If no images exist, it defaults to the alligator
        images4question=quiz.questions[iq].imageList
        #iImage counts the images for the i-th question
        for iImage in range(len(images4question)):
            imagePath="./bank/images/"+images4question[iImage]
            questionImages=Image.open(imagePath)
            questionImages=questionImages.resize((100, 100), Image.ANTIALIAS)
            png1=ImageTk.PhotoImage(questionImages)

            #WIDGET firstImage !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! !!!!!!!!
            firstImage=tk.Label(self.frame,
                                image=png1,
                                height=100,
                                width=100,
                                bg="white")

            firstImage.image=png1
            firstImage.grid(row=iq+1+iImage,column=imageCol)

    def firstWidgets(self):
        global TESTNUMBERCOUNT, SELECTEDQUIZ
        quizNumberCount=tk.IntVar()
        testNumberCount=tk.IntVar()
        testNumberCount.set(TESTNUMBERCOUNT)        
        print_s="Print "+ courseName+'-'+testName
     
    #WIDGET printButt (top row) prints test
        printButt=tk.Button(self.frame,
            text=print_s,
            fg="red", bg="white",font=('',fontSize),
            command=self.master.destroy)
        printButt.grid(sticky='w',row=0, column=quizNameCol)
    
        #List bank quizzes (spans 2 columns, after NtestCol)
        #   iQ is an index for quizzes, iq indexes the questions
        for iQ in range(len(quizList)):
            q_number=csvInfo.Nquestions[iQ] #x is total # questions on quiz
            quizname_s=str(iQ+1)+': '+quizList[iQ]+' ('+str(q_number)+')'
    #WIDGETS pdfNum  prints quiznames w number questions          
            pdfNum=tk.Button(self.frame,
              text=quizname_s,  fg="green",                           
              bg=pickQuizColor(iQ),
              borderwidth="2",
              relief="groove",
              font=('',fontSize),
              command=partial(self.selectQuiz, iQ, quizNumberCount))
            pdfNum.grid(sticky="w", row=iQ+1, column=quizNameCol,
                                                      columnspan=2)
    def updateCsv(self,iQ,iq,onTest,quizNumberCount):
        #called when the inQuizCol Checkbutton is hit
        global TESTNUMBERCOUNT,NtestCol,SELECTEDQUIZ
        if onTest.get():
            csvInfo.choices[iQ][iq]="1"
            old=quizNumberCount.get()
            quizNumberCount.set(old+1)
            TESTNUMBERCOUNT+=1
        else:
            csvInfo.choices[iQ][iq]="0"
            old=quizNumberCount.get()
            quizNumberCount.set(old-1)
            TESTNUMBERCOUNT=TESTNUMBERCOUNT-1  

    #WIDGET testNumberCount #number questions on Test
        Ntest=tk.Label(self.frame,
           bg="white",
           text=str(TESTNUMBERCOUNT),                      
           font=('bold',fontSize+2))
        Ntest.grid(row=0, column=NtestCol)

    def selectQuiz(self, iQ,quizNumberCount):
        global SELECTEDQUIZ
        SELECTEDQUIZ=iQ
      #Opens the selected quiz and creates question Checkbutton
        #Get quiznames from quizList and create a quiz for each:
        quiz = makeQuiz('./bank/'+quizList[iQ]+'.tex',quizList[iQ])
        #Destroy widgets in inQuizCol column:
        for item in self.frame.grid_slaves():
            if int(item.grid_info()["column"])==inQuizCol:
                   item.destroy()  #grid_forget() also works
        #Destroy widgets in preCol column: (does not seem to work)
        for item in self.frame.grid_slaves():
            if int(item.grid_info()["column"])==preCol:
                   item.destroy()  #grid_forget() also works
                  
        #Count questions from each quiz that will be on test
        quizNumberCount=tk.IntVar()
        quizNumberCount.set(0)
        for iq in range(quiz.Nquestions):
            print('iQ,iq,choices',iQ,iq,csvInfo.choices[iQ][iq])
            
    #WIDGET NfromQ counts num questions from selected quiz 1st row  
        NfromQ=tk.Label(self.frame, bg="white", font=('',fontSize+2),
            textvariable=quizNumberCount)
        NfromQ.grid(row=0, column=inQuizCol)      
        for iq in range(quiz.Nquestions):
            onTest=tk.BooleanVar()       
            onTest.set(csvInfo.choices[iQ][iq])                   
            if csvInfo.choices[iQ][iq]=="1":
                temp=quizNumberCount.get()
                quizNumberCount.set(temp+1)
                
    #WIDGET firstCol counts number questions from quiz 1st col
            firstCol=tk.Label(self.frame,
                font=('',fontSize),bg="white",
                 textvariable=quizNumberCount)
            firstCol.grid(row=iQ+1, column=NtestCol)
            
    #WIDGETS  choicesWidget toggle question ON/OFF
            choicesWidget=tk.Checkbutton(self.frame,
                text="ON", font=('',fontSize),                         
                background="green", foreground="green",
                variable=onTest,                                     
                indicatoron=False,                                                  
                command=partial(self.updateCsv, iQ, iq,
                                        onTest, quizNumberCount)                                         )        
            choicesWidget.grid(sticky='w' ,row=iq+1, column=inQuizCol)

        print('iQ, quizNumberCount',iQ,quizNumberCount.get())
        
        ## Create quesCol for each question in selected quiz
        # first destroy widgets already present in quesCol
        for item in self.frame.grid_slaves():
            if int(item.grid_info()["column"])==quesCol:
                   item.destroy()  #grid_forget() also works
        # s is of form #:quizname (labeled iQ+1 so as to start at 1)
        s=str(iQ+1)+": "+quizList[iQ]

    #WIDGET quesHead places quiz's name on top row
        quesHead=tk.Button(self.frame,text=s,
            borderwidth="2", bg="yellow",
            font=('',fontSize), relief="groove",
            command=self.alsoDestroy)
        quesHead.grid(sticky='w',row=0,column=quesCol)
        # The selected quiz has now been placed in the top row of quesCol
        # Next we place short fagment from question in the other rows:        
        questionList=quiz.questions

        for iq in range(quiz.Nquestions):          
            fragment=quiz.questions[iq].Q[0]
            fragment=cleanFragment(fragment, lengthOfPreview)
          
    #WIDGETS showPreview creates button with preview (fragment)
            showPreview=tk.BooleanVar()

        #Widget quesBox    (see also Widget first Image below
            quesBox=tk.Button(self.frame,
               font=('',fontSize),
               text=str(iQ+1)+'.'+str(iq+1)+': '+fragment,                          
                command = partial(self.questionPreview, iQ, iq,
                                                      showPreview))
            quesBox.grid(sticky="w", row=iq+1, column=quesCol)


######################################  Add new functions here
            

#######################################################################
## Program Starts here ## Program Starts here ## Program Starts here
#######################################################################

if __name__ == "__main__":
    global SELECTEDQUIZ
    SELECTEDQUIZ=0
        
    timeStamp=str(int(time.time()*100))
    [courseName,quizList]=getCourse()
    testName=input('Enter testName for '+courseName+': ')
    timeName = timeStamp+'-'+courseName+'-'+testName

    for n in range(len(quizList)):
        quiz = makeQuiz('./bank/'+quizList[n]+'.tex',quizList[n])
        whatis('quizList',quizList[n])
        csvInfo.comments.append(str(n+1))
        csvInfo.quizNames.append(quiz.quizName)
        #selections = # questions for given quiz that appear on test
        csvInfo.selections.append(str(0)) #string for export to csv
        csvInfo.Nquestions.append(quiz.Nquestions)
        csvInfo.types.append(quiz.quizType)
        choiceList=[]
        #choice is "1" if on test, else it is "0"
        for i in range(quiz.Nquestions):
            choiceList.append(str(0))
        csvInfo.choices.append(choiceList)
    TESTNUMBERCOUNT=0    
    root=tk.Tk()
    root.geometry(mainWindowSize(MAINX,MAINY))
    Application(root).pack(side="top", fill="both", expand=True)
    root.mainloop()

################################# done for now #################
    #iterate to define csvOut   
    csvOut=[]    
    for iQ in range(len(quizList)):
        row=[str(iQ)]
        row.append(csvInfo.quizNames[iQ])
        row.append(csvInfo.selections[iQ])
        row.append(csvInfo.Nquestions[iQ])
        Niq=csvInfo.Nquestions[iQ]
        print('Niq',Niq)
        for iq in range(Niq):
            print(iQ,csvInfo.quizNames[iQ],csvInfo.choices[iQ][iq])
            row.append(csvInfo.choices[iQ][iq])
        csvOut.append(row)
                   


